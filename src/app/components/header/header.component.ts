import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  //Vars
  public menu: Array<object>;

  constructor() { }

  ngOnInit() {
    this.menu = [
      {
        title: 'Dashboard',
        route: '/dashboard'
      },
      {
        title: 'Cadastrar',
        route: '/cadastro'
      }
    ]
  }

}
