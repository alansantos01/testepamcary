import { CadastroRoutingModule } from './cadastro.routing.module';
import { CadastroComponent } from './cadastro.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule, MatFormFieldModule, MatIconModule, MatButtonModule, MatToolbarModule, MatSelectModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { ReactiveFormsModule} from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [
    CadastroComponent
  ],
  imports: [
    CommonModule,
    MatInputModule,
    MatFormFieldModule,
    CadastroRoutingModule,
    MatIconModule,
    LayoutModule,
    MatButtonModule,
    MatToolbarModule,
    ReactiveFormsModule,
    MatSelectModule,
    NgxMaskModule.forRoot()
  ]
})
export class CadastroModule { }
