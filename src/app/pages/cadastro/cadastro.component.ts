import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.scss']
})
export class CadastroComponent implements OnInit {
  public newForm: FormGroup;
  public date:any = new Date;
  public data: any;
  
  constructor(private formBuilder: FormBuilder) { 
    this.newForm = this.formBuilder.group({
      nome: [null, [Validators.required, this.validateLettlers]],
      titulo: [null, [Validators.required, this.validateLettlers]],
      email: [null, [Validators.required, Validators.email]],
      fone: [null],
      assunto: [null, [Validators.required]],
      message: [null, [Validators.required]]
    })
  }

  ngOnInit() {
  }

  sendMessage(){
    let object = {
      title: this.newForm.get('titulo').value,
      name: this.newForm.get('nome').value,
      message: this.newForm.get('message').value,
      subject: this.newForm.get('assunto').value,
      fone: this.newForm.get('fone').value,
      time: this.date.getDate() + "/" + (this.date.getMonth() + 1) + "/" + this.date.getFullYear()
    };
    this.data = JSON.parse(sessionStorage.getItem('cards'));
    this.data.push(object);
    sessionStorage.removeItem('cards');
    sessionStorage.setItem('cards', JSON.stringify(this.data));
    alert('Enviado com sucesso!')
  }

  validateLettlers(input: FormControl){
    let value = input.value; 
    const regex = /^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/;
    input.hasError('Lettlers')
    return (regex.test(value) ? null : {Lettlers: true });
  }

}
