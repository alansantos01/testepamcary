import { Card } from './../../models/card';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public cards: Array<any>;
  public message: any;
  example: Array<Card>;
  lengthData: number;

  constructor() { }

  ngOnInit() {
    if(sessionStorage.getItem('cards') != null){
      this.refreshCards();
      this.lengthData = this.cards.length;
    } else {
      this.example = [
        {
          title: 'Mensagem do dia',
          name: '',
          message: 'Você tem de melhorar seu modo de enxergar a vida, amigo! Eu gosto muito de você e é por esse motivo que estou dando minha opinião. Não fique aborrecido comigo, por favor!',
          subject: 'Mensagem de felicidade',
          fone: '',
          time: '05/07/2020'
        },
        {
          title: 'Eu to aki2',
          name: '',
          message: 'Olha ele ai',
          subject: 'Ele',
          fone: '',
          time: 'now'
        },
        {
          title: 'Eu to aki3',
          name: '',
          message: 'Olha ele ai',
          subject: 'Ele',
          fone: '',
          time: 'now'
        }
      ];
      sessionStorage.setItem('cards', JSON.stringify(this.example));
      this.refreshCards();
      this.lengthData = this.cards.length;
    }
  }

  searchMessage(id){
    this.message = this.cards[id - 1];
    this.message.id = id;
  }

  refreshCards(){
    this.cards = JSON.parse(sessionStorage.getItem('cards'));
  }

  sendToTrash(id){
    this.cards.splice(id,1);
    sessionStorage.removeItem('cards');
    sessionStorage.setItem('cards',JSON.stringify(this.cards));
    this.refreshCards();
    alert('Item' + id + 'excluido com sucesso!');
  }

}
